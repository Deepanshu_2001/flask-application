from email.policy import default
from enum import unique
from sqlalchemy import create_engine as ce
from flask import Flask
from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from sqlalchemy.orm import relationship
from sqlalchemy import Integer, ForeignKey
app = Flask(__name__)

# mysql_engine = ce("mysql://root:Deepanshu@123@localhost:3306/Organisation_Application_DB")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://root:Deepanshu@123@localhost:3306/Organisation_Application_DB"
db = SQLAlchemy(app)


class login(db.Model):
    employeeid = db.Column(db.Integer, primary_key=True, nullable=False)
    email = db.Column(db.String(80), nullable=False, unique=True)
    password_1 = db.Column(db.String(50), nullable=False, unique=True)
    Deleted = db.Column(db.Boolean, default=False)
    Creation_date = db.Column(db.DateTime, default=datetime.utcnow)
    Updation_date = db.Column(db.DateTime, default=datetime.utcnow)


class post(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    employeeid = db.Column(db.Integer,ForeignKey("employeeid_login"), nullable=False)
    employeeid_login = relationship("login", foreign_keys=[employeeid])
    Designation = db.Column(db.String(50), nullable=False)
    Deleted = db.Column(db.Boolean, default=False)
    dateOfCreation  = db.Column(db.DateTime, default=datetime.utcnow)
    dateOfUpdation = db.Column(db.DateTime, default=datetime.utcnow)

class employee_data(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    employeeid = db.Column(db.Integer,ForeignKey("employeeid_login"), nullable=False)
    employeeid_data = relationship("login", foreign_keys=[employeeid])
    Name1 = db.Column(db.String(50), nullable=False)
    Designation = db.Column(db.String(50), nullable=False)
    PhoneNumber = db.Column(db.Integer, unique=True, nullable=False)
    BloodGroup  = db.Column(db.String(5))
    PermanentAddress = db.Column(db.String(500),  nullable=False)
    CurrentAddress = db.Column(db.String(500),  nullable=False)
    FatherName = db.Column(db.String(50),  nullable=False)
    MotherName = db.Column(db.String(50),  nullable=False)
    dateOfCreation  = db.Column(db.DateTime, default=datetime.utcnow)
    dateOfUpdation = db.Column(db.DateTime, default=datetime.utcnow)

class inOutSummery(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    employeeid = db.Column(db.Integer,ForeignKey("employeeid_login"), nullable=False)
    employeeid_inOutSummary = relationship("login", foreign_keys=[employeeid])
    date_1 = db.Column(db.DateTime, default=datetime.utcnow)
    inTime = db.Column(db.DateTime, default=datetime.utcnow)
    OutTime = db.Column(db.DateTime, default=datetime.utcnow)
    SummaryOfTask = db.Column(db.String(600), nullable=False)
    dateOfCreation = db.Column(db.DateTime, default=datetime.utcnow)
    dateOfUpdation = db.Column(db.DateTime, default=datetime.utcnow)

app.run(debug=True)














