CREATE DATABASE Organisation_Application_DB;
USE Organisation_Application_DB;
CREATE TABLE login(employeeid int NOT NULL PRIMARY KEY,
					email varchar(50) unique NOT NULL,
                    password_1 varchar(50) unique,
                    Deleted boolean default false,
                    Creation_date timestamp default now() NOT NULL,
                    Updation_date timestamp default now() NOT NULL
                    );

SELECT * FROM post;

CREATE TABLE post(id int primary KEY,
					employeeid int NOT NULL,
                    Designation varchar(25) NOT NULL,
                    dateOfCreation timestamp default now() NOT NULL,
                    dateOfUpdation timestamp default now() NOT NULL,
                    CONSTRAINT fk_post
    				FOREIGN KEY(employeeid) 
					REFERENCES login(employeeid));


SELECT * FROM post;

CREATE TABLE employee_data(id int PRIMARY KEY auto_increment,
							employeeid int NOT NULL,
                            Name1 varchar(30) NOT NULL,
                            Designation varchar(30) NOT NULL,
                            PhoneNumber numeric(15) NOT NULL,
                            BloodGroup varchar(10),
                            PermanentAddress varchar(100) NOT NULL,
                            CurrentAddress varchar(100) NOT NULL,
                            FatherName varchar(30) NOT NULL,
                            MotherName varchar(30) NOT NULL,
                            Creation_date timestamp default now() NOT NULL,
                            Updation_date timestamp default now() NOT NULL,
                            CONSTRAINT fk_employee_data
							FOREIGN KEY(employeeid) 
							REFERENCES login(employeeid));
SELECT * FROM employee_data;

CREATE TABLE inOutSummery(id int PRIMARY KEY,
							employeeid int NOT NULL,
                            date_1 datetime default now() NOT NULL,
                            inTime timestamp default now(),
                            OutTime timestamp default now(),
                            SummaryOfTask text NOT NULL,
                            dateOfCreation timestamp default now() NOT NULL,
                            dateOfUpdation timestamp default now() NOT NULL,
                            CONSTRAINT fk_inOutSummery1_from_table_post
							FOREIGN KEY(employeeid) 
							REFERENCES post(employeeid),
                            CONSTRAINT fk_inOutSummery1_from_table_employee_date
							FOREIGN KEY(employeeid) 
							REFERENCES employee_data(employeeid));
SELECT * FROM inOutSummery;

